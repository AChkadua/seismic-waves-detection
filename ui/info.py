from tkinter import *

import i18n

from config import Setting


class InfoWindow(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.title(i18n.t("info.name"))
        self.geometry("500x500")
        self.resizable(False, False)

        self.container = Canvas(self, borderwidth=0)
        self.container.grid(row=0, column=0, sticky="nsew")

        self.frame = Frame(self.container)
        self.frame.grid(row=0, column=0, sticky="nsew")
        self.frame.bind('<Configure>', self.on_configure)

        self.scroll = Scrollbar(self, orient="vertical")
        self.scroll.grid(row=0, column=1, sticky="nsew")
        self.scroll.config(command=self.container.yview)

        self.container.configure(yscrollcommand=self.scroll.set)
        self.container.create_window((4, 4), window=self.frame, anchor="nw", tags="self.frame")

        header_font: str = "Times 14 bold"
        self.labels = []
        for section in ["main", "basic", "onsets", "settings"]:
            self.labels.append(Label(self.frame, justify=LEFT, text=i18n.t(f"info.{section}.name"), font=header_font))
            self.labels.append(Label(self.frame, justify=LEFT, text=i18n.t(f"info.{section}.content"), anchor="w"))
            if section is "settings":
                for setting in Setting:
                    self.labels.append(Label(self.frame, justify=LEFT,
                                             text=i18n.t(f"info.{section}.list.{setting.config_name}"), anchor="w"))
        for i in range(0, len(self.labels)):
            self.labels[i].grid(row=i, column=0, sticky="nsew", padx=10, pady=10)
            self.labels[i].config(wraplength=450)
        Grid.columnconfigure(self, 0, weight=1)
        Grid.rowconfigure(self, 0, weight=1)

    def on_configure(self, event):
        self.container.configure(scrollregion=self.container.bbox("all"))
