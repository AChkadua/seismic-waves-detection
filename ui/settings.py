from copy import deepcopy
from tkinter import *
from tkinter import messagebox, filedialog

import i18n

from config import Setting


class SettingsWindow(Tk):
    def __init__(self, context):
        Tk.__init__(self)
        self.context = context
        self.title(i18n.t("settings.name"))
        self.geometry("800x650")
        self.resizable(False, False)
        analysis_label = Label(self, text=i18n.t("settings.analysis.name"))
        analysis_label.grid(row=0, column=0, columnspan=2, padx=10, pady=10)
        other_label = Label(self, text=i18n.t("settings.other.name"))
        other_label.grid(row=0, column=2, columnspan=2, padx=10, pady=10)
        self.entries = {}
        i = 1
        for setting in [s for s in Setting if s.prefix == "OTHER"]:
            label = Label(self, text=i18n.t(f"settings.other.{setting.config_name}"), justify=LEFT, anchor="w")
            label.grid(row=i, column=2, padx=10, pady=10, sticky="new")
            label.config(wraplength=180)

            if setting is Setting.LANG:
                var = StringVar(self, self.context[setting])
                lang_selector = OptionMenu(self, var, "en", "ru")
                lang_selector.grid(row=i, column=3, padx=10, pady=10, sticky="new")
                self.entries[setting] = var
            elif setting is Setting.ROOT_DIR or setting is Setting.DB_PATH:
                panel = Frame(self)
                var = StringVar(self, self.context[setting])
                label = Label(panel, text=context[setting], anchor=W, width=12)
                browse = Button(panel, text=i18n.t("settings.browse"), width=8,
                                command=lambda s=setting, v=var: self.show_filedialog(s, v))
                label.grid(row=0, column=0, sticky="ns")
                browse.grid(row=0, column=1, sticky="new")
                Grid.rowconfigure(panel, 0, weight=1)
                Grid.rowconfigure(panel, 1, weight=1)
                panel.grid(row=i, column=3, padx=10, pady=10, sticky="new")
                self.entries[setting] = var
            else:
                entry = Entry(self, textvariable=StringVar(self, self.context[setting]))
                entry.grid(row=i, column=3, padx=10, pady=10, sticky="new")
                self.entries[setting] = entry
            i += 1

        i = 1
        for setting in [s for s in Setting if s.prefix == "ANALYSIS"]:
            label = Label(self, text=i18n.t(f"settings.analysis.{setting.config_name}"), justify=LEFT, anchor="w")
            label.grid(row=i, column=0, padx=10, pady=10, sticky="new")
            label.config(wraplength=180)

            entry = Entry(self, textvariable=StringVar(self, self.context[setting]))
            entry.grid(row=i, column=1, padx=10, pady=10, sticky="new")
            self.entries[setting] = entry
            i += 1

        reset_button = Button(self, text=i18n.t("settings.reset"), command=self.reset, width=10)
        reset_button.grid(row=i, column=2, padx=10, pady=10, sticky="es")
        save_button = Button(self, text=i18n.t("settings.save"), command=self.save, width=10)
        save_button.grid(row=i, column=3, padx=10, pady=10, sticky="es")

        for col in range(0, 4):
            Grid.columnconfigure(self, col, weight=1)
        rows = max(len(context.settings["ANALYSIS"].options()), len(context.settings["OTHER"].options()))
        for i in range(0, rows + 1):
            Grid.rowconfigure(self, i, weight=1)

    def save(self):
        require_restart = [Setting.PROC_COUNT, Setting.DB_MONITOR_INT, Setting.DB_PATH, Setting.CHANNELS]
        require_app_restart = [Setting.LANG]
        cur = deepcopy(self.context.settings)
        message = None
        for setting in require_restart:
            if str(self.context[setting]) != self.entries[setting].get():
                message = i18n.t("settings.require_restart")
        for setting in require_app_restart:
            if str(self.context[setting]) != self.entries[setting].get():
                message = i18n.t("settings.require_app_restart")
        for (key, value) in self.entries.items():
            cur[key.prefix][key.config_name] = key.getter(value.get())
        if message is not None:
            messagebox.showwarning(title=i18n.t("settings.warning"), message=message, parent=self)
        self.context.set_settings(cur)
        self.destroy()

    def reset(self):
        for key, entry in self.entries.items():
            if key is Setting.LANG:
                entry.set(Setting.LANG.default)
            else:
                entry.delete(0, END)
                entry.insert(0, key.default)

    def show_filedialog(self, setting, var):
        if setting is Setting.ROOT_DIR:
            filename = filedialog.askdirectory(parent=self)
            var.set(filename)
        elif setting is Setting.DB_PATH:
            filename = filedialog.askopenfilename(parent=self, defaultextension=".mdb",
                                                  filetypes=((i18n.t("file.accdb"), "*.accdb"),
                                                             (i18n.t("file.accdb"), "*.mdb"),
                                                             (i18n.t("file.all"), "*.*")))
            var.set(filename)
