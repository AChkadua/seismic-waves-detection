import csv
from datetime import datetime
from tkinter import *
from tkinter import messagebox, filedialog
from tkinter.ttk import Treeview
from os.path import isfile, isdir

import i18n

from config import Setting
from ui.info import InfoWindow
from ui.settings import SettingsWindow


class MainWindow(Tk):

    def __init__(self, context, start_func, stop_func, reset_func):
        Tk.__init__(self)
        self.context = context
        context.main_window = self
        self.title(i18n.t("window.header"))
        self.geometry("700x500")
        self.minsize(420, 400)

        def on_close():
            stop_func()
            self.destroy()

        self.protocol("WM_DELETE_WINDOW", on_close)
        main_label = Label(self, text=i18n.t("window.main_label"))

        main_label.grid(row=0, column=0, columnspan=3, sticky="w", padx=10, pady=10)

        self.management = Controls(self, context, start_func, stop_func, reset_func)
        self.management.grid(row=1, column=0, columnspan=3, sticky="nsew", padx=10, pady=10)
        self.onsets_frame = OnsetsFrame(self)
        self.onsets_frame.grid(row=2, column=0, columnspan=3, sticky="nsew", padx=10)

        for i in range(0, 3):
            Grid.columnconfigure(self, i, weight=1)
        for i in range(2, 3):
            Grid.rowconfigure(self, i, weight=1)

        if not isfile(context[Setting.DB_PATH]):
            messagebox.showerror(i18n.t("settings.error"), i18n.t("settings.wrong_wsg_db_path"))
        elif not isdir(context[Setting.ROOT_DIR]):
            messagebox.showerror(i18n.t("settings.error"), i18n.t("settings.wrong_root_dir_path"))

    def show_processing(self, filename):
        self.onsets_frame.show_processing(filename)

    def show_result(self, result):
        self.onsets_frame.show_result(result)

    def settings_updated(self):
        self.management.refresh_controls()
        if not isfile(self.context[Setting.DB_PATH]):
            messagebox.showerror(i18n.t("settings.error"), i18n.t("settings.wrong_wsg_db_path"))
        elif not isdir(self.context[Setting.ROOT_DIR]):
            messagebox.showerror(i18n.t("settings.error"), i18n.t("settings.wrong_root_dir_path"))


class Controls(Frame):
    def __init__(self, master, context, start_func, stop_func, reset_func):
        Frame.__init__(self, master)
        self.context = context

        start_btn_text = StringVar(self, i18n.t("controls.start"))
        stop_btn_text = StringVar(self, i18n.t("controls.stopped"))

        self.start = Button(self, textvariable=start_btn_text,
                            state=NORMAL if isfile(context[Setting.DB_PATH]) and
                                            isdir(context[Setting.ROOT_DIR]) else DISABLED)
        self.stop = Button(self, textvariable=stop_btn_text, state=DISABLED)

        def on_start():
            start_func()
            start_btn_text.set(i18n.t("controls.started"))
            stop_btn_text.set(i18n.t("controls.stop"))
            self.start.config(state=DISABLED)
            self.stop.config(state=NORMAL)

        def on_stop():
            stop_func()
            start_btn_text.set(i18n.t("controls.start"))
            stop_btn_text.set(i18n.t("controls.stopped"))
            self.start.config(state=NORMAL)
            self.stop.config(state=DISABLED)

        self.start.config(command=on_start)
        self.stop.config(command=on_stop)
        self.start.grid(row=0, column=0, sticky="ew", padx=10)
        self.stop.grid(row=0, column=1, sticky="ew", padx=10)
        open_settings = Button(self, text=i18n.t("controls.settings"), command=self.show_settings)
        open_settings.grid(row=0, column=2, sticky="ew", padx=10)
        info = Button(self, text=i18n.t("controls.info"), command=Controls.show_info)
        info.grid(row=0, column=3, sticky="ew", padx=10)

        if reset_func is not None:
            reset = Button(self, text=i18n.t("controls.reset"), command=reset_func)
            reset.grid(row=0, column=4, sticky="ew", padx=10)

        for i in range(0, 5 if reset_func is not None else 4):
            Grid.columnconfigure(self, i, minsize=100)

    @staticmethod
    def show_info():
        info_window = InfoWindow()
        info_window.focus_force()
        info_window.mainloop()

    def show_settings(self):
        settings_window = SettingsWindow(self.context)
        settings_window.focus_force()
        settings_window.mainloop()

    def refresh_controls(self):
        self.start.config(state=NORMAL if isfile(self.context[Setting.DB_PATH]) and
                                          isdir(self.context[Setting.ROOT_DIR]) else DISABLED)


class OnsetsFrame(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.onsets = Treeview(self)
        self.onsets["columns"] = ("time", "q1", "q2", "status")
        self.onsets.heading("#0", text=i18n.t("table.file"), anchor="w")
        self.onsets.heading("time", text=i18n.t("table.time"), anchor="w")
        self.onsets.heading("q1", text=i18n.t("table.q1"), anchor="w")
        self.onsets.heading("q2", text=i18n.t("table.q2"), anchor="w")
        self.onsets.heading("status", text=i18n.t("table.status"), anchor="w")
        self.onsets.column("#0", anchor="w", minwidth=200)
        for col in self.onsets["columns"]:
            self.onsets.column(col, anchor="w", minwidth=30, width=30)
        scroll = Scrollbar(self, orient="vertical", command=self.onsets.yview)
        onsets_label = Label(self, text=i18n.t("table.name"))
        onsets_label.grid(row=0, column=0, columnspan=2, sticky="w", pady=5)
        self.onsets.grid(row=1, column=0, sticky="nsew")
        scroll.grid(row=1, column=1, sticky="nsew")
        self.onsets.config(yscrollcommand=scroll.set)
        onsets_management = OnsetsControls(self, self.delete, self.clear, self.dump)
        onsets_management.grid(row=2, column=0, columnspan=2, sticky="nsew", pady=10)

        for i in range(0, 1):
            Grid.columnconfigure(self, i, weight=1)
        Grid.rowconfigure(self, 1, weight=1)
        Grid.rowconfigure(onsets_management, 0, weight=1)

    def show_processing(self, filename):
        self.onsets.insert("", "end", text=filename, values=("", "", "", i18n.t("status.processing")))

    def show_result(self, result):
        item = self.__search_by_text(result.filename)
        if result.exception is None:
            if len(result.onsets) > 0:
                idx = self.onsets.index(item)
                self.onsets.delete(item)
                for onset in result.onsets:
                    time = datetime.fromtimestamp(onset[0]).strftime("%d-%m-%Y %H:%M:%S")
                    self.onsets.insert("", idx, text=result.filename,
                                       values=(time, onset[1], onset[2], i18n.t("status.ok")))
                    idx += 1
            else:
                self.onsets.item(item, values=("", "", "", i18n.t("status.ok")))
        else:
            self.onsets.item(item, values=("", "", "", i18n.t("status.error")))

    def delete(self):
        selected = self.onsets.selection()[0]
        if selected is not None:
            self.onsets.delete(selected)

    def clear(self):
        self.onsets.delete(*self.onsets.get_children())

    def dump(self):
        filename = filedialog.asksaveasfilename(defaultextension=".csv",
                                                filetypes=((i18n.t("file.csv"), "*.csv"), (i18n.t("file.all"), "*.*")))
        if filename is not None:
            items = self.onsets.get_children()
            with open(filename, "w", encoding="utf-8") as file:
                writer = csv.writer(file, delimiter="\t", quotechar="\"")
                headers = [self.onsets.heading(col)["text"] for col in self.onsets["columns"]]
                headers.insert(0, self.onsets.heading("#0"))
                writer.writerow(tuple(headers))
                for item in items:
                    file = self.onsets.item(item, "text")
                    time, q1, q2, status = self.onsets.item(item, "values")
                    writer.writerow((file, time, q1, q2, status))

    def __search_by_text(self, text):
        for child in self.onsets.get_children():
            if self.onsets.item(child, "text") == text:
                return child


class OnsetsControls(Frame):
    def __init__(self, master, delete_func, clear_func, dump_func):
        Frame.__init__(self, master)
        delete = Button(self, text=i18n.t("table.controls.delete"), command=delete_func)
        delete_all = Button(self, text=i18n.t("table.controls.clear"), command=clear_func)
        dump = Button(self, text=i18n.t("table.controls.save"), command=dump_func)
        delete.grid(row=1, column=0, sticky="new", padx=10)
        delete_all.grid(row=1, column=1, sticky="new", padx=10)
        dump.grid(row=1, column=2, sticky="new", padx=10)
        for i in range(0, 3):
            Grid.columnconfigure(self, i, minsize=125)
