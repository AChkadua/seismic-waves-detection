import pyodbc


class AccessConnector:

    def __init__(self, location):
        self.__location = location
        self.__connection = None
        self.__cursor = None
        self.__is_connected = False

    def connect(self):
        if self.__location is None:
            raise Exception("Не указано местоположение файла")

        self.__connection = pyodbc.connect(
            r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};' + f'DBQ={self.__location};')
        self.__is_connected = True
        self.__cursor = self.__connection.cursor()

    def execute(self, sql, *params):
        if self.__cursor is None:
            raise Exception("Нет подключения к базе")
        return FetchResult(self.__cursor.execute(sql, params))

    def disconnect(self):
        self.__cursor.close()
        self.__connection.close()
        self.__is_connected = False

    def is_connected(self):
        return self.__is_connected


class FetchResult:
    def __init__(self, cursor):
        self.__cursor = cursor

    def fetchone(self):
        if self.__cursor is None:
            raise Exception("Отсутствует курсор")
        return self.__cursor.fetchone()

    def fetchall(self):
        if self.__cursor is None:
            raise Exception("Отсутствует курсор")
        return self.__cursor.fetchall()
