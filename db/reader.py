import re
from enum import Enum
from multiprocessing import Lock
from multiprocessing.pool import Pool
from os import path
from struct import unpack
from threading import Thread
from time import sleep, time, timezone

from numpy import array, mean

from config import Setting
from db.connector import AccessConnector
from processing.processor import Processor


class DBReader(Thread):

    def __init__(self, context):
        Thread.__init__(self)
        self.__conn = AccessConnector(context[Setting.DB_PATH])
        self.__delay = context[Setting.DB_MONITOR_INT]
        self.__last_ts_lock = Lock()
        self.__last_ts = None
        self.__processors = Pool(processes=context[Setting.PROC_COUNT])
        self.__context = context
        self.__monitor_query = "select cpath, tbegin, sps, datatype from filelist where tbegin > {}"
        if context[Setting.CHANNELS].strip() is not "":
            channels = [f"'{c.upper()}'" for c in re.split("\\s*,\\s*", context[Setting.CHANNELS])]
            self.__monitor_query += f" and UCase(ch) in ({', '.join(channels)})"
        self.disconnect = False

    def run(self):
        self.__conn.connect()
        # за точку отсчёта берём tbegin последнего записанного файла или текущее время в UTC, если база пустая
        self.__last_ts = self.__conn.execute("select max(tbegin) from filelist").fetchall()[0][0]
        self.__last_ts = self.__last_ts if self.__last_ts is not None else (time() - timezone)
        while not self.disconnect:
            try:
                self.__last_ts_lock.acquire()
                new_fetches = self.__conn.execute(self.__monitor_query.format(self.__last_ts))
            finally:
                self.__last_ts_lock.release()
            rows = new_fetches.fetchall()
            if len(rows) > 0:
                for row in rows:
                    self.__context.start_processing(path.join(self.__context[Setting.ROOT_DIR], row[0]))
                    print(row)
                    self.__processors.apply_async(
                        Processor.process,
                        (path.join(self.__context[Setting.ROOT_DIR], row[0]), int(row[1]), int(row[2]), int(row[3]),
                         self.__context.settings["ANALYSIS"],),
                        callback=self.__context.report)
                self.__last_ts = max(list(map(lambda t: int(t[1]), rows)))
            sleep(self.__delay)
        self.__processors.terminate()
        self.__conn.disconnect()
        return

    def reset(self):
        try:
            self.__last_ts_lock.acquire()
            self.__last_ts = 0
        finally:
            self.__last_ts_lock.release()


class DataType(Enum):
    INT = ("h", 2)
    LONG = ("l", 4)
    FLOAT = ("f", 4)
    DOUBLE = ("d", 8)
    SHORT = ("h", 2)

    def __init__(self, unpack_type, bytes_length):
        self.unpack_type: str = unpack_type
        self.bytes_length: int = bytes_length

    def __str__(self):
        return self.name

    @staticmethod
    def get_by_index(item):
        if item is 1:
            return DataType.INT
        elif item is 2:
            return DataType.LONG
        elif item is 3:
            return DataType.FLOAT
        elif item is 4:
            return DataType.DOUBLE
        elif item is 5:
            return DataType.SHORT
        else:
            raise KeyError(f"Неизвестный тип данных: {item}")

    def parse(self, string):
        return unpack(self.unpack_type, string)[0]


class SeismicFileReader:

    @staticmethod
    def read(file, datatype):
        reader = open(file, "rb")
        try:
            header = SeismicFileReader.readheader(reader.read(512))
            datatype = DataType.get_by_index(datatype)

            values = []
            val = reader.read(datatype.bytes_length)
            while val:
                values.append(datatype.parse(val))
                val = reader.read(datatype.bytes_length)
            arr = array(values)
            return arr - mean(arr), header
        finally:
            reader.close()

    @staticmethod
    def readheader(string):
        lines = list(map(lambda l: l.decode(encoding='UTF-8'), string.split(b"\r\n")[1:-1]))
        params = {}
        for line in lines:
            key, value = line.split("=")
            params[key] = value
        return params
