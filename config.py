import logging
import traceback
from enum import Enum
from multiprocessing import cpu_count

import i18n


class GlobalContext:
    __instance = None

    def __init__(self, settings):
        if GlobalContext.__instance is None:
            GlobalContext.__instance = self

            # по умолчанию максимальное число обработчиков файлов равно половине числа ядер процессора
            proc_count_conf = Setting.PROC_COUNT
            if proc_count_conf.getter(settings[proc_count_conf.prefix][proc_count_conf.config_name].value) is -1:
                settings[proc_count_conf.prefix][proc_count_conf.config_name] = proc_count_conf.default

            self.settings = settings
            self.reader = None
            self.connector = None
            self.main_window = None
        else:
            raise Exception("Cannot initialize a second singleton instance")

    def __getitem__(self, item):
        return item.getter(self.settings[item.prefix][item.config_name].value)

    def set_settings(self, val):
        self.settings = val
        with open("config.ini", "w") as file:
            self.settings.write(file)
        self.main_window.settings_updated()

    @staticmethod
    def initialize(settings):
        if GlobalContext.__instance is None:
            GlobalContext(settings)
        return GlobalContext.__instance

    @staticmethod
    def getinstance():
        if GlobalContext.__instance is None:
            raise Exception("Global context not initialized")
        return GlobalContext.__instance

    def start_processing(self, filename):
        self.main_window.show_processing(filename)

    def report(self, result):
        if result is not None:
            if self.main_window is None:
                logging.error("Main window not set!")
                exit(0)
            self.main_window.show_result(result)
            if result.exception is None:
                logging.info(i18n.t("logging.success", file=result.filename, time=result.end_time - result.start_time))
            else:
                ex = result.exception
                trace = traceback.format_exception(type(ex), ex, tb=ex.__traceback__)
                logging.error(i18n.t("logging.failure", file=result.filename, time=result.end_time - result.start_time,
                                     trace=''.join(trace)))


class Setting(Enum):
    SMOOTH_WINDOW = ("ANALYSIS", "smoothing_window", 1.5, float)
    MAX_DELAY = ("ANALYSIS", "max_onset_delay", 2.0, float)
    LOC_MIN_CRIT = ("ANALYSIS", "local_minimum_criteria", 0.005, float)
    POSS_MISTAKE = ("ANALYSIS", "possible_mistake", 2.0, float)
    Q1_WINDOW = ("ANALYSIS", "q1_time_window", 0.1, float)
    NOISE_WINDOW = ("ANALYSIS", "noise_window", 2.0, float)
    SIGNAL_WINDOW = ("ANALYSIS", "signal_window", 0.5, float)
    ENVELOPE_WINDOW = ("ANALYSIS", "envelope_window", 10.0, float)
    REQUIRED_RATIO = ("ANALYSIS", "required_ratio", 1.75, float)

    DB_MONITOR_INT = ("OTHER", "db_monitor_interval", 1, int)
    DB_PATH = ("OTHER", "wsg_db_path", "C:\\WSG\\DATABASE\\wsg.mdb", str)
    ROOT_DIR = ("OTHER", "root_directory", "C:\\WSG\\Data", str)
    PROC_COUNT = ("OTHER", "max_process_count", int(cpu_count() / 2), int)
    LANG = ("OTHER", "language", "en", str)
    CHANNELS = ("OTHER", "channels", "", str)

    def __init__(self, prefix, config_name, default, getter):
        self.prefix = prefix
        self.config_name = config_name
        self.default = default
        self.getter = getter


def firstnotnone(first, second):
    res = first if first is not None else second
    if second is None:
        raise ValueError("All values are None")
    return res
