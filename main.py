import logging
from sys import argv

import i18n
from configupdater import ConfigUpdater

from config import GlobalContext, Setting
from db.reader import DBReader
from ui.mainwindow import MainWindow


def start():
    context = GlobalContext.getinstance()
    if context.reader is None:
        context.reader = DBReader(context)
        context.reader.start()


def stop():
    context = GlobalContext.getinstance()
    if context.reader is not None and context.reader.is_alive():
        context.reader.disconnect = True
        context.reader.join()
        context.reader = None


def reset():
    context = GlobalContext.getinstance()
    if context.reader is not None and context.reader.is_alive():
        context.reader.reset()


if __name__ == "__main__":
    is_debug = "--debug" in argv
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    handler = logging.FileHandler('messages.log', 'w', 'utf-8')
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s - %(message)s'))
    root_logger.addHandler(handler)

    i18n.set("filename_format", "{locale}.{format}")
    i18n.load_path.append("./i18n/")
    settings = ConfigUpdater()
    with open("config.ini") as file:
        settings.read_file(file)

    try:
        ctxt = GlobalContext.initialize(settings)
    except FileNotFoundError:
        ctxt = None
    i18n.set("locale", ctxt[Setting.LANG])

    w = MainWindow(ctxt, start, stop, reset if is_debug else None)
    w.mainloop()
