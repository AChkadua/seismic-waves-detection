from collections import deque

from numpy import log10, mean, sum, sqrt, abs, ceil, array, double
from scipy.stats import moment, kurtosis

from config import Setting


class StatisticAnalyzer:

    def __init__(self, df, width, settings):
        self.df = df
        self.width = width
        self.window = deque(maxlen=width)
        self.cf = deque(maxlen=width)
        self.scf = deque(maxlen=width)
        self.aic = deque(maxlen=width)
        self.saic = deque(maxlen=width)
        self.last_moment2, self.last_moment4 = 0., 0.
        self.appr_onset, self.aic_min, self.s_aic_min = None, None, None
        self.cf_min, self.s_cf_min = None, None
        self.lcf_min = (None, None)
        self.rcf_min = (None, None)
        self.onsets, self.q1, self.q2 = [], [], []
        self.smoothing_window = double(settings[Setting.SMOOTH_WINDOW.config_name].value)
        self.max_onset_delay = double(settings[Setting.MAX_DELAY.config_name].value)
        self.local_minima_criterion = double(settings[Setting.LOC_MIN_CRIT.config_name].value)
        self.possible_mistake = double(settings[Setting.POSS_MISTAKE.config_name].value)
        self.q1_time_window = double(settings[Setting.Q1_WINDOW.config_name].value)
        self.noise_window = double(settings[Setting.NOISE_WINDOW.config_name].value)
        self.signal_window = double(settings[Setting.SIGNAL_WINDOW.config_name].value)
        self.smoothing_int = int(ceil(self.smoothing_window * self.df))

    def next(self, time, value):
        self.window.append(value)
        self.cf.append(self.kurtosis())
        self.scf.append(moving_smooth(self.cf, self.smoothing_int))

        if len(self.cf) > 1:
            self.aic.append(aic(array(self.cf), len(self.cf) - 1))
            self.saic.append(moving_smooth(self.aic, self.smoothing_int))
        else:
            self.aic.append(0)
            self.saic.append(0)

        # approximate onset lookup preparations
        if len(self.saic) > 2:
            if self.is_local_minima(self.saic[-3], self.saic[-2], self.saic[-1]):
                self.s_aic_min = time
            if self.is_local_minima(self.aic[-3], self.aic[-2], self.aic[-1]):
                self.aic_min = time

        # approximate onset lookup
        if all_not_none(self.s_aic_min, self.aic_min) \
                and abs(self.s_aic_min - self.aic_min) < self.possible_mistake:
            self.appr_onset = min(self.aic_min, self.s_aic_min)
            self.s_aic_min = None
            self.aic_min = None

        if len(self.cf) > 1:
            if self.is_local_minima2(self.cf[-2], self.cf[-1]):
                self.cf_min = time
            if self.is_local_minima2(self.scf[-2], self.scf[-1]):
                self.s_cf_min = time

            if self.appr_onset is None:
                # exact onset lookup preparations
                if all_not_none(self.s_cf_min, self.cf_min) \
                        and abs(self.s_cf_min - self.cf_min) < self.possible_mistake:
                    self.lcf_min = (min(self.cf_min, self.s_cf_min), self.cf[-1])
            else:
                # exact onset lookup
                if time - self.appr_onset < self.max_onset_delay:
                    if all_not_none(self.s_cf_min, self.cf_min) \
                            and abs(self.s_cf_min - self.cf_min) < self.possible_mistake:
                        self.rcf_min = (min(self.cf_min, self.s_cf_min), self.cf[-1])

                    if all_not_none(self.lcf_min[0], self.rcf_min[0]) \
                            and self.appr_onset - self.lcf_min[0] < self.max_onset_delay:
                        self.onsets.append(self.rcf_min[0]
                                           if self.lcf_min[1] > self.rcf_min[1]
                                           else self.lcf_min[0])
                        self.appr_onset = None
                        self.lcf_min = self.rcf_min
                        self.rcf_min = (None, None)
                else:
                    self.appr_onset = None
                    self.lcf_min = self.rcf_min
                    self.rcf_min = (None, None)

        i = -1
        while -i < len(self.onsets) and self.onsets[i] > time - self.signal_window:
            i -= 1

        if len(self.onsets) > 0 and self.onsets[i] + self.signal_window == time:
            signal_start = int(-self.signal_window * self.df)
            noise_start = int(-(self.noise_window + self.signal_window) * self.df)

            values_array = array(self.window, dtype=float)
            slope = abs((self.cf[signal_start + int(self.q1_time_window * self.df)] - self.cf[signal_start]) /
                        self.q1_time_window)
            signalrms = sqrt(mean(values_array[signal_start:] ** 2))
            noiserms = sqrt(mean(values_array[noise_start:signal_start - 1] ** 2))

            self.q1.append(log10(slope / noiserms))
            self.q2.append(20 * log10(signalrms / noiserms))

    def fast_moment2(self):
        if len(self.window) == self.width:
            self.last_moment2 = moment(self.window, 2)
        else:
            self.last_moment2 = self.last_moment2 - self.window[0] ** 2 + self.window[-1] ** 2
        return self.last_moment2

    def fast_moment4(self):
        if len(self.window) == self.width:
            self.last_moment4 = moment(self.window, 4)
        else:
            self.last_moment4 = self.last_moment4 - self.window[0] ** 4 + self.window[-1] ** 4
        return self.last_moment4

    def kurtosis(self):
        if len(self.window) >= self.width:
            m2 = self.fast_moment2()
            m4 = self.fast_moment4()
            return -3 if m2 == 0 else m4 / (m2 ** 2)
        else:
            return kurtosis(self.window)

    def is_local_minima2(self, prev_elem, cur_elem):
        if prev_elem > 0:
            return cur_elem < (1 + self.local_minima_criterion) * prev_elem or cur_elem < 0
        else:
            min_elem = min(cur_elem, prev_elem)
            return cur_elem - min_elem + 1 < (1 + self.local_minima_criterion) * (prev_elem - min_elem + 1)

    @staticmethod
    def is_local_minima(prev_elem, cur_elem, next_elem):
        return cur_elem < prev_elem and cur_elem < next_elem


def moving_smooth(arr, n):
    return mean(list(arr)[-n:])


def aic(arr, k):
    arrlen = len(arr)
    squared = arr ** 2
    return k * log10(mean(squared[:k])) + (arrlen - k + 1) * log10(sum(squared[k:]) / (arrlen - k + 1))


def all_not_none(*args):
    return not any(a is None for a in args)
