from time import time

from numpy import abs, mean
from scipy.signal import hilbert

from config import Setting
from processing.analyzer import StatisticAnalyzer


class Processor:

    @staticmethod
    def process(file, start_ts, df, datatype, settings):
        start = time()
        try:
            from db.reader import SeismicFileReader
            arr, _ = SeismicFileReader.read(file, datatype)
            analyzer = StatisticAnalyzer(df, 3 * 60 * df, settings)
            per = 1 / df
            ts = start_ts
            for e in arr:
                analyzer.next(ts, e)
                ts += per
            onsets = list(zip(analyzer.onsets, analyzer.q1, analyzer.q2))
            for onset in onsets:
                onset_idx = (onset[0] - start_ts) * df
                pre = int(min(settings[Setting.EVAL_WINDOW], onset[0] - start_ts))
                post = int(min(settings[Setting.EVAL_WINDOW], ts - onset[0]))
                envelope = abs(hilbert(arr[onset_idx - pre:onset_idx + post]))
                if (mean(envelope[post:]) / mean(envelope[:pre])) < settings[Setting.REQUIRED_RATIO]:
                    onsets.remove(onset)
            end = time()
            return ProcessingResult(file, start, end, onsets)
        except Exception as e:
            end = time()
            print(e)
            return ProcessingResult(file, start, end, exception=e)


class ProcessingResult:
    def __init__(self, filename, proc_start, proc_end, onsets=None, exception=None):
        self.onsets = onsets
        self.filename = filename
        self.start_time = proc_start
        self.end_time = proc_end
        self.exception = exception
